trap 'echo $I ; echo $I >> resultat' INT ALRM
trap 'echo "je sors" ; exit' TERM

I=1
while :		# la commande «~\texttt{:}~» renvoie toujours \emph{vrai}
do
    sleep 1	# attendre 1 seconde
    I=$((I+1))
done
