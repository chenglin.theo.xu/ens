pid_t v ;
int x = 1 ;

v = fork () ;
switch (v)
{
  case -1 : // erreur
    raler ("fork") ;

  case 0 :  // fils
    x = 5 ;
    break ;

  default : // père
    x = 3 ;
    break ;
}
