void verrouiller (void) {
  sigset_t s ;

  sigemptyset (&s) ;
  sigaddset (&s, SIGUSR1) ;
  sigprocmask (SIG_BLOCK, &s, NULL) ;	// pthread\_sigmask pour les threads
}

void deverrouiller (void) {
  sigset_t s ;

  sigemptyset (&s) ;
  sigaddset (&s, SIGUSR1) ;
  sigprocmask (SIG_UNBLOCK, &s, NULL) ;
}
